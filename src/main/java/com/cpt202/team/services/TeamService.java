package com.cpt202.team.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

import com.cpt202.team.model.Team;
import com.cpt202.team.repositories.TeamRepo;

@Service
public class TeamService {
    @Autowired
    private TeamRepo teamRepo;
    
    public Team newTeam(Team team){
       return teamRepo.save(team);
    }
// public Team updTeam(Team team){

// }
    public List<Team> getTeamList(){
        return teamRepo.findAll();
    }

    public List<Team> clearTeamList(){
        teamRepo.deleteAll();
        return teamRepo.findAll();
    }
    
    public List<Team> deleteOneTeam(Team team){
        teamRepo.delete(team);
        return teamRepo.findAll();
    }
}
