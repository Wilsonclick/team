package com.cpt202.team.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private int memberCount;//member_count

    
    public Team() {
    }
    public Team(int id,String name, int memberCount) {
        this.name = name;
        this.memberCount = memberCount;
        this.id=id;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getMemberCount() {
        return memberCount;
    }
    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }


public int getId() {
    return id;
}
public void setId(int id) {
    this.id = id;
}
// @Override
// public String toString(){
//     String teamInfo="Name is "+name+" memberCount is "+memberCount;
//     return teamInfo;
// }
}
